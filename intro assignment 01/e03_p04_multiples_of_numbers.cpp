#include <iostream>
using namespace std;

void multiplos_operador_ternario(int n) {
  cout << "usando operador ternario - ingresá -666 para terminar" << endl;
//  while (true) {
  while (n != -666) {
    cout << "número?: ";
    cin >> n;
    cout << (((n >= 0) && (n % 5 == 0)) ? (n / 5) : -1) << endl;
  }
}

void multiplos_continue(int n) {
  cout << "usando continue - ingresá -666 para terminar" << endl;
//  while (true) {
  while (n != -666) {
    cout << "número?: ";
    cin >> n;
    if ((n < 0) || (n % 5 != 0)) {
      continue;
    }
    cout << (n / 5) << endl;
  }
}

void multiplos_break(int n) {
  cout << "usando break - ingresá -666 para terminar terminar" << endl;
  while (true) {
    cout << "número?: ";
    cin >> n;
    if (n == -666) {
      break;
    }
    if ((n >= 0) && (n % 5 == 0)) {
      cout << (n / 5) << endl;
    }
  }
  cout << "Goodbye!" << endl;
}

int main() {
  int n;
  
  multiplos_operador_ternario(n);

  multiplos_continue(n);
  
  multiplos_break(n);
  
	return 0;
}
