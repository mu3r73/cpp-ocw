#include <iostream>
using namespace std;

void print_hello_world(const char *hw) {
	cout << hw << endl;
}

void print_hello_world_for_loop(const char *hw, int times) {
	cout << endl << "for loop:" << endl;
	for (int i = 0; i < times; i++) {
		cout << hw << endl;
	}
}

void print_hello_world_while_loop(const char *hw, int times) {
	cout << endl << "while loop:" << endl;
	int i = 0;
	while (i++ < times) {
		cout << hw << endl;
	}
}

void print_hello_world_do_while_loop(const char *hw, int times) {
	cout << endl << "do...while loop:" << endl;
	int i = 0;
	do {
		cout << hw << endl;
	} while (++i < times);
}

int main() {
	const char *hw = "Hello, World!";

	// simply output it
	print_hello_world(hw);

	// request number of times
	cout << "times to say hw?: ";
	int times;
	cin >> times;

	// print it times times
	print_hello_world_for_loop(hw, times);
	print_hello_world_while_loop(hw, times);
	print_hello_world_do_while_loop(hw, times);

	return 0;
}
