#include <iostream>
#include <climits>
using namespace std;

int main() {
  int n, x;
  double mean;
  int mini, maxi;
 
  cout << "cant de números?: ";
  cin >> n;

  mean = 0;
  mini = INT_MAX; 
  maxi = INT_MIN; 

  for (int i = 0; i < n; i++) {
    cout << "número?: ";
    cin >> x;
    
    mean += x;
    mini = min(mini, x);
    maxi = max(maxi, x);
  } 
  
  cout << "mean = " << (mean * 1.0 / n) << endl;
  cout << "min = " << mini << endl;
  cout << "max = " << maxi << endl;
  cout << "range = " << (maxi - mini) << endl;

	return 0;
}
