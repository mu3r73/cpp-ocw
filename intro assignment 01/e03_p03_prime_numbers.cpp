#include <iostream>
using namespace std;

// algorithm stolen from:
// http://en.wikipedia.org/wiki/Primality_test
bool is_prime(int n) {
  if (n <= 1) {
    return false;
  }
  if (n <= 3) {
    return true;
  }
  if ((n % 2 == 0) || (n % 3 == 0)) {
    return false;
  }
  int i = 5;
  while (i * i <= n) {
    if ((n % i == 0) || (n % (i+2) == 0)) {
      return false;
    }
    i += 6;
  }
  return true;
}

void show_first_n_primes(int n) {
  cout << "primes: ";
  int i = 0;
  int primes = 0;
  while (primes < n) {
    if (is_prime(i)) {
      cout << " " << i;
      primes++;
    }
    i++;
  }
  cout << endl;
}

int main() {
  int n;

  cout << "número?: ";
  cin >> n;
  
  show_first_n_primes(n);
  
	return 0;
}
